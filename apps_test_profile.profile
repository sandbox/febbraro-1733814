<?php
/**
 * @file
 * Enables modules and site configuration for a standard site installation.
 */


/**
 * Test install of apps from a profile
 */
function apps_test_profile_install_tasks(&$install_state) {

  // Start it off
  $tasks = array();

  // Summon the power of the Apps module
  require_once(drupal_get_path('module', 'apps') . '/apps.profile.inc');

  // Set up the  Apps install task
  $panopoly_server = array(
    'machine name' => 'panopoly',
    'default apps' => array(
      'panopoly_admin',
    ),
    'required apps' => array(
      'panopoly_admin',
    ),
  );
  $tasks = $tasks + apps_profile_install_tasks($install_state, $panopoly_server);
  return $tasks;
}

/**
 * Implements hook_appstore_stores_info()
 */
function apps_test_profile_apps_servers_info() {
  $profile = variable_get('install_profile', 'apps_test_profile');
  $info =  drupal_parse_info_file(drupal_get_path('profile', $profile) . '/' . $profile . '.info');
  return array(
    'panopoly' => array(
      'title' => 'Panopoly',
      'description' => 'Apps for Panopoly',
      'manifest' => (empty($info['version']) || $info['version'] == '7.x-1.x-dev') ? 'http://apps.getpantheon.com/panopoly-dev' : 'http://apps.getpantheon.com/panopoly',
    ),
  );
}

/**
 * Implements hook_form_FORM_ID_alter(). Fill in testing data
 */
function apps_test_profile_form_install_configure_form_alter(&$form, $form_state) {
  // Testing data
  $form['site_information']['site_name']['#default_value'] = 'Apps Test Profile';
  $form['admin_account']['account']['name']['#default_value'] = 'admin';
  $form['server_settings']['site_default_country']['#default_value'] = 'US';
  $form['server_settings']['date_default_timezone']['#default_value'] = 'America/New_York'; // West coast, best coast
  $form['site_information']['site_mail']['#default_value'] = 'app@test.com';
  $form['admin_account']['account']['mail']['#default_value'] = 'app@test.com';
  $form['update_notifications']['update_status_module']['#default_value'] = array(1);
}

/**
 * Implements hook_form_FORM_ID_alter()
 */
function apps_test_profile_form_apps_profile_apps_select_form_alter(&$form, $form_state) {
  // Remove the demo content selection .
  $form['default_content_fieldset']['#access'] = FALSE;
}
